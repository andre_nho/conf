" activate syntax highlighting
filetype plugin indent on
syntax on
set background=dark

" usage options
set nowrap
set scrolloff=3
set sidescrolloff=5
set wildmode=longest,list:longest
set completeopt=menu,menuone,longest

" status line
set ruler

" changing tabe with ALT+,/.
map  <M-,> :tabp<CR>
imap <M-,> <ESC>:tabp<CR>
map  <M-.> :tabn<CR>
imap <M-.> <ESC>:tabn<CR>
map  <ESC>[5;3~ :tabp<CR>
imap <ESC>[5;3~ <ESC>:tabp<CR>
map  <ESC>[6;3~ :tabn<CR>
imap <ESC>[6;3~ <ESC>:tabn<CR>
tmap  <M-,> :tabp<CR>
tmap  <M-.> :tabn<CR>

" some specific words (TODO - not working)
syn keyword Special buffer self

" jump to last know position
autocmd BufReadPost * 
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif

"
" bugfixes
"

" correct CTRL-PGUP/DOWN in rxvt
nmap <ESC>[5^ <C-PageUp>
nmap <ESC>[6^ <C-PageDown>
nmap <ESC><ESC>[5^ <C-PageUp>
nmap <ESC><ESC>[6^ <C-PageDown>
imap <ESC>[5^ <C-PageUp>
imap <ESC>[6^ <C-PageDown>
imap <ESC><ESC>[5^ <C-PageUp>
imap <ESC><ESC>[6^ <C-PageDown>

" movement with wrap activated
noremap  <buffer> <silent> k gk
noremap  <buffer> <silent> j gj
noremap  <buffer> <silent> 0 g0
noremap  <buffer> <silent> $ g$

" transparent fold
hi Folded ctermbg=none

" fold functions
function! MyFoldText()
    let lines = printf('%' . len(line('$')) . 'd', v:foldend - v:foldstart + 1)
    let line  = substitute(foldtext(), '^+-\+ *\d\+ lines: ', '', '')

    return '+  [' . lines . ' lines: ' . line . ']  '
endfunction

set foldtext=MyFoldText()
set fillchars="fold: "

" C highlighting
let c_hi_identifiers = 'all'
let c_hi_libs = ['*']

" remove initial message
set shortmess+=I
