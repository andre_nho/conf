" Vim color file

" Set 'background' back to the default.  The value can't always be estimated
" and is then guessed.
hi clear Normal
set bg&

" Remove all existing highlighting and set the defaults.
hi clear

" Load the syntax highlighting defaults, if it's enabled.
if exists("syntax_on")
  syntax reset
endif

let colors_name = "andredark"

" ------------------------------------------------

set background=dark

hi Identifier ctermfg=172 cterm=bold
hi Type       ctermfg=77  cterm=bold
hi Statement  ctermfg=11
hi PreProc    ctermfg=106 cterm=bold
hi Search     ctermfg=7   ctermbg=8 cterm=standout
hi Constant   ctermfg=146
hi Comment    ctermfg=14
hi Folded     ctermfg=14  ctermbg=234
hi CursorLine cterm=none ctermbg=black

" vim: sw=2
